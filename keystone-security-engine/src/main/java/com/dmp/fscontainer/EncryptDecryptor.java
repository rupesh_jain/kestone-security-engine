package com.dmp.fscontainer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

import android.util.Log;

    public class EncryptDecryptor {
        Cipher ecipher;
        Cipher dcipher;

        public EncryptDecryptor(String passPhrase) {
            
            
         // 8-byte Salt
            byte[] salt = {
                (byte)0xA9, (byte)0x9B, (byte)0xC8, (byte)0x32,
                (byte)0x56, (byte)0x35, (byte)0xE3, (byte)0x03
            };

            // Iteration count
            int iterationCount = 19;
            
           
            try {
            	// Create the key
                KeySpec keySpec = new PBEKeySpec(passPhrase.toCharArray(), salt, iterationCount);
                
                SecretKey key = SecretKeyFactory.getInstance(
                "PBEWithMD5AndDES").generateSecret(keySpec);
                
                ecipher = Cipher.getInstance(key.getAlgorithm());
                dcipher = Cipher.getInstance(key.getAlgorithm());

             // Prepare the parameter to the ciphers
                AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);

                // CBC requires an initialization vector
                ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
                dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
            } catch (java.security.InvalidAlgorithmParameterException e) {
            } catch (javax.crypto.NoSuchPaddingException e) {
            } catch (java.security.NoSuchAlgorithmException e) {
            } catch (java.security.InvalidKeyException e) {
            } catch (InvalidKeySpecException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
        }
        
     // Buffer used to transport the bytes from one stream to another
        byte[] buf = new byte[1024];

        
        public boolean encrypt(String  Sin, String Sout) {
        	
            try {
            	
            	InputStream in = new FileInputStream(Sin);
            	OutputStream out = new FileOutputStream(Sout);
            	
/*            	InputStream in = new ByteArrayInputStream(Sin);
//            	OutputStream out = new FileOutputStream(Sout);
            	OutputStream out;
            	ByteArrayOutputStream newout = new ByteArrayOutputStream();
//            	newbuf = newout.toByteArray();
*/            	
            	// Bytes written to out will be encrypted
                out = new CipherOutputStream(out, ecipher);

                // Read in the cleartext bytes and write to out to encrypt
                int numRead = 0;
                while ((numRead = in.read(buf)) >= 0) {
                    out.write(buf, 0, numRead);
                }
                out.close();
                in.close();
//                String hin;
//                IOUtils.copy(in, out);                
                                
                return true;
            } catch (java.io.IOException e) {
            	
            	Log.d("DMP","Encrypt IO Exception");
            	e.printStackTrace();
//                 deleteFile1(Sout);
                 return false;
            }
        }

        byte[] interbuf = new byte[1024];
        byte[] encbuf = new byte[220992];
        
        public byte[] encrypt(byte[]  Sin) {
        	
            try {
            	
            	InputStream in = new  ByteArrayInputStream(Sin);
            	OutputStream out = new  ByteArrayOutputStream();
            	
/*            	InputStream in = new ByteArrayInputStream(Sin);
//            	OutputStream out = new FileOutputStream(Sout);
            	OutputStream out;
            	ByteArrayOutputStream newout = new ByteArrayOutputStream();
//            	newbuf = newout.toByteArray();
*/            	
            	// Bytes written to out will be encrypted
                out = new CipherOutputStream(out, ecipher);

                // Read in the cleartext bytes and write to out to encrypt
                int numRead = 0;
                while ((numRead = in.read(interbuf)) >= 0) {
                    out.write(interbuf, 0, numRead);
                }
                out.close();
                in.close();
//                String hin;
//                IOUtils.copy(in, out);                
                out.write(encbuf);
                   
                return encbuf;
            } catch (java.io.IOException e) {
            	
//                 deleteFile1(Sout);
                 e.printStackTrace();
                 return encbuf;
            	//return false;
            }
        }
        
        private void deleteFile1(String FileName)
        {
        	// A File object to represent the filename
            File f = new File(FileName);
            f.delete();
        }
        
        public boolean decrypt(String Sin, String Sout) {
        	
            try {
            	
            	InputStream in = new FileInputStream(Sin);
            	OutputStream out = new FileOutputStream(Sout);
                // Bytes read from in will be decrypted
                in = new CipherInputStream(in, dcipher);

                // Read in the decrypted bytes and write the cleartext to out
                int numRead = 0;
                while ((numRead = in.read(buf)) >= 0) {
                    out.write(buf, 0, numRead);
                }
                out.close();
                in.close();
                
                return true;
            } catch (java.io.IOException e) {
            	Log.d("DMP","decrypt IO Exception");
            	// delete the newly generated wrong file
            	e.printStackTrace();
            	deleteFile1(Sout);

            	return false;
            }
        }
        
        byte[] decbuf = new byte[220992];        
        public byte[] decrypt(byte[] Sin) {
        	
            try {
            	
            	InputStream in = new ByteArrayInputStream(Sin);
            	OutputStream out = new ByteArrayOutputStream();
                // Bytes read from in will be decrypted
                in = new CipherInputStream(in, dcipher);

                // Read in the decrypted bytes and write the cleartext to out
                int numRead = 0;
                while ((numRead = in.read(buf)) >= 0) {
                    out.write(buf, 0, numRead);
                }
                out.close();
                in.close();
                out.write(decbuf);
                return decbuf;
            } catch (java.io.IOException e) {
            	Log.d("Encrypt-Decrypt Activity","decrypt IO Exception");
            	// delete the newly generated wrong file
            	e.printStackTrace();
//            	deleteFile1(Sout);

            	return decbuf;
            }
        }
        
        public void encrypt(InputStream is, OutputStream os) {
        	        try {
        	            byte[] buf = new byte[1024];
        	            // bytes at this stream are first encoded
        	            os = new CipherOutputStream(os, ecipher);
        	            // read in the clear text and write to out to encrypt
        	            int numRead = 0;
        	            while ((numRead = is.read(buf)) >= 0) {
        	                os.write(buf, 0, numRead);
        	            }
        	            // close all streams
        	            os.close();
        	        }
        	        catch (IOException e) {
        	            System.out.println("I/O Error:" + e.getMessage());
        	        }
        	    }
        
        public void decrypt(InputStream is, OutputStream os) {
            try {
                byte[] buf = new byte[1024];
                // bytes read from stream will be decrypted
                CipherInputStream cis = new CipherInputStream(is, dcipher);
                // read in the decrypted bytes and write the clear text to out
                int numRead = 0;
                while ((numRead = cis.read(buf)) >= 0) {
                    os.write(buf, 0, numRead);
                }
                // close all streams

                cis.close();
                is.close();
                os.close();
            }catch (IOException e) {
                System.out.println("I/O Error:" + e.getMessage());
            }
    
        }
    }
	
//}
