package com.dell;

import java.security.SecureRandom;
import java.util.UUID;

import android.content.Context;
import android.content.ContextWrapper;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.dell.mdmframework.db.AuthDatabaseHelper;
import com.dell.mdmframework.utils.SimpleCrypto;

public class Uid {

	private static String _UniQueKey = null;
	private static Context _AppContext = null;
	private static String _EncryptionKey = null;
	
	
	public static void InitUid(Context aContext)
	{
		_AppContext = aContext.getApplicationContext();
	}
	
	
	
	
	public static String getUid() {

		return "ajdfkjaldsjf";
	}

	public static String getUniqueKey(Context aContext) {
		
		if(_UniQueKey!=null)
			return _UniQueKey;
		
		final TelephonyManager tm = (TelephonyManager) ((ContextWrapper) aContext)
				.getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);

		final String tmDevice, tmSerial, androidId;
		tmDevice = "" + tm.getDeviceId();
		tmSerial = "" + tm.getSimSerialNumber();
		androidId = ""
				+ android.provider.Settings.Secure.getString(
						aContext.getContentResolver(),
						android.provider.Settings.Secure.ANDROID_ID);

		// Log.e("UID", "TM Device"+tmDevice +" TM Serial"+tmSerial
		// +" Android ID "+androidId);
		UUID deviceUuid = new UUID(androidId.hashCode(),
				((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
		String deviceId = "AMXaavvzzBB" + tmDevice + androidId + "123$!~@#";// deviceUuid.toString();
		Log.e("UID", "" + deviceId);

		_UniQueKey = deviceId;
		return deviceId;

	}

	public void verifyLocalPassword() {

	}

	private static void setEncKey(Context aContext, String lSeed) {
		String lKey = Generate128BitRandom();
		// ankit if there is hyphen in key then sql cipher gives crash
		while (lKey.indexOf("-") != -1) {
			lKey = Generate128BitRandom();
		}

		_EncryptionKey = lKey;
		AuthDatabaseHelper dbHelper = new AuthDatabaseHelper(aContext);
		try {
			String lEncKey = SimpleCrypto.encrypt(lSeed, lKey);
			if (lEncKey != null) {
				dbHelper.setEncryptionKey(lEncKey);

			}
			dbHelper.close();
		} catch (Exception e) {
			dbHelper.close();
			e.printStackTrace();
		}

	}

	private static String Generate128BitRandom() {
		/*
		 * int numBits = 128; SecureRandom random = new SecureRandom();
		 * BigInteger randomNumber = new BigInteger(numBits, random);
		 * 
		 * return randomNumber.toString();
		 */

		byte[] r = new byte[16]; // Means 128 bit
		SecureRandom random = new SecureRandom();
		random.nextBytes(r);

		String randomString = byteToHexString(r);// new String(r);

		return randomString;
	}

	private static String byteToHexString(byte[] aBytes) {
		// Log.i("MDMClient","InitialBytes"+aBytes.toString());
		StringBuffer hexString = new StringBuffer();
		// String hexString = new String();
		for (int i = 0; i < aBytes.length; i++) {
			String hex = Integer.toHexString(0xFF & aBytes[i]);
			if (hex.length() == 1) {
				// could use a for loop, but we're only dealing with a single
				// byte
				hexString.append('0');
			}
			hexString.append(hex);
		}
		// Log.i("MDMClient","Converted string "+hexString+" Length: "+hexString.length());

		return hexString.toString();
	}

	public static String getEncKey(Context aContext) {
		
		if(_EncryptionKey!=null)
			return _EncryptionKey;
		
		Context lContext = null;
		
		if(aContext == null)
			lContext = _AppContext;
		else
			lContext = aContext;
		
		String lSeed = getUniqueKey(aContext);
		String lEncKey = null;
		String lKey = null;
		AuthDatabaseHelper dbHelper = new AuthDatabaseHelper(aContext);
		try {

			lEncKey = dbHelper.getEncryptionKey();
			if (lEncKey != null)
			{
				lKey = SimpleCrypto.decrypt(lSeed, lEncKey);
			}
			else // enc key is not present so create new one.
			{
				setEncKey(aContext,lSeed);
				lEncKey = dbHelper.getEncryptionKey();
				if (lEncKey != null)
					lKey = SimpleCrypto.decrypt(lSeed, lEncKey);
			}
			// dbHelper.close();

		} catch (Exception e) {
			// dbHelper.close();
			e.printStackTrace();
		}

		finally {
			dbHelper.close();
		}
		
		_EncryptionKey = lKey;
		return lKey;

	}

}
/*// Set the filesystem and db encryption key. 
// When device password resetted, this must be set.
String lKey = Generate128BitRandom();
//ankit if there is hyphen in key then sql cipher gives crash
while (lKey.indexOf("-") != -1){
	lKey = Generate128BitRandom();
}
lEncKey = SimpleCrypto.encrypt(masterKey , lKey);
if (lEncKey != null) {
	dbHelper.setEncryptionKey(lEncKey);
	
	SharedPreferences prefs = getSharedPreferences(
			"com.dell.mdmframework.key",
			Context.MODE_WORLD_READABLE);
	SharedPreferences.Editor editor = prefs.edit();
	editor.putString("enc_key", lEncKey);
	String lEncPassKey = "akdjkajdkklajd!!@#@##AADWEKJFKJ12233" ;
	String lEncPassword = SimpleCrypto.encrypt(lLocalPassword,lEncPassKey);
	editor.putString("local_password", lEncPassword);
	editor.putString("email_username",SimpleCrypto.encrypt(masterKey ,"ankit@dmpmail.onmicrosoft.com"));
	editor.putString("email_domain", SimpleCrypto.encrypt(masterKey ,"dmpmail.onmicrosoft.com"));
	editor.putString("email_server",SimpleCrypto.encrypt(masterKey ,"pod51024.outlook.com"));
	editor.commit();
}*/