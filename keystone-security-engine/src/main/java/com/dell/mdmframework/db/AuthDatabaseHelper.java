package com.dell.mdmframework.db;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.dell.mdmframework.utils.SimpleCrypto;

public class AuthDatabaseHelper extends SQLiteOpenHelper {

	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "AuthManager.db";

	// password table name
	private static final String TABLE_AUTH = "auth";
	private static final String TABLE_PASS = "localpass";
	private static final String TABLE_ENCRPTION_KEY = "encryptionkey";
	private static final String TABLE_EMAIL = "email_param";
	private static final String TABLE_LAUNCHER = "launcher_AppList";
	private static final String TABLE_SETTINGS = "settings_parameters";
	private static final String TABLE_DEVICE_ID = "device_id";
	
	// password Table Columns names
	private static final String KEY_ID = "id";
	private static final String KEY_NAME = "username";
	private static final String KEY_PASSWORD = "password";
	private static final String KEY_SSO_TOKEN = "token";
	private static final String KEY_LOCAL_PASS = "localpass";
	private static final String KEY_LAST_ACCESS_TIME = "accesstime";

	private static final String KEY_FSDB_ENCRYPTION_KEY = "fsdbenckey";
	
	private static final String KEY_EMAIL_USERNAME = "email_username";
	private static final String KEY_EMAIL_PASSWORD = "email_password";
	private static final String KEY_EMAIL_DOMAIN = "email_domain";
	private static final String KEY_EMAIL_SERVER = "email_server";

	
	// launcher related
	private static final String KEY_VERSION_NUM = "launcher_VerNum";
	private static final String KEY_PACKAGE_NAME = "launcher_PkdName";
	private static final String KEY_INSTALL_STATUS = "launcher_InstallStatus";
	private static final String KEY_RESERVED = "launcher_Reserved";

	

	private static final String KEY_ENROL_USERID = "settings_enrol_userid";
	private static final String KEY_IS_DEVICE_LOCKED = "isDevice_Locked";
	private static final String KEY_TIMEOUT_DETECTED = "timeout_detected";
	private static final String KEY_TIME_VALUE = "settings_time";
	private static final String KEY_LAST_USED_AUTH_URL = "base_auth_url";
	
	private static final String KEY_DEVICEID = "device_id";

	private static final String KEY_USERNAME_MAIN = "main_username";
	

	public AuthDatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		Log.d("AuthDBHelper","After super call");

	}

	//creating database
	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_AUTH + "("
		+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
		+ KEY_PASSWORD + " TEXT,"+ KEY_SSO_TOKEN + " TEXT," + "UNIQUE("+KEY_NAME+","+KEY_PASSWORD+") ON CONFLICT REPLACE)";
		

		String CREATE_LOCALPASS_TABLE = "CREATE TABLE " + TABLE_PASS + "("
		+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_LOCAL_PASS + " TEXT,"
		+ KEY_LAST_ACCESS_TIME + " TEXT)";

		
		String CREATE_ENCRYPTION_KEY_TABLE = "CREATE TABLE " + TABLE_ENCRPTION_KEY + "("
		+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_FSDB_ENCRYPTION_KEY +" TEXT)";
		
		String CREATE_EMAIL_PARAM_TABLE = "CREATE TABLE " + TABLE_EMAIL + "("
				+ KEY_EMAIL_USERNAME + " TEXT NOT NULL," + KEY_EMAIL_PASSWORD +" TEXT NOT NULL,"
				+ KEY_EMAIL_DOMAIN + " TEXT NOT NULL," + KEY_EMAIL_SERVER + " TEXT NOT NULL"
				+");";
		
		String CREATE_LAUNCHER_TABLE = "CREATE TABLE " + TABLE_LAUNCHER + "("
				+ KEY_PACKAGE_NAME + " TEXT NOT NULL," + KEY_VERSION_NUM +" TEXT NOT NULL,"
				+ KEY_INSTALL_STATUS + " TEXT NOT NULL," + KEY_RESERVED + " TEXT NOT NULL"
				+");";
		
		String CREATE_SETTINGS_TABLE = "CREATE TABLE " + TABLE_SETTINGS + "("
				+ KEY_ENROL_USERID + " TEXT NOT NULL," + KEY_IS_DEVICE_LOCKED +" INT NOT NULL,"
				+ KEY_TIME_VALUE + " TEXT NOT NULL," + KEY_TIMEOUT_DETECTED +" INT NOT NULL,"+KEY_LAST_USED_AUTH_URL
				 +" TEXT NOT NULL,"+ KEY_USERNAME_MAIN + " TEXT NOT NULL"+");";
		
		String CREATE_DEVICEID_TABLE = "CREATE TABLE " + TABLE_DEVICE_ID + "("
		+ KEY_DEVICEID + " TEXT NOT NULL"+");";
		
		//db.execSQL(CREATE_CONTACTS_TABLE);
		db.execSQL(CREATE_CONTACTS_TABLE);
		db.execSQL(CREATE_LOCALPASS_TABLE);    
		db.execSQL(CREATE_ENCRYPTION_KEY_TABLE);
		db.execSQL(CREATE_EMAIL_PARAM_TABLE);
		db.execSQL(CREATE_LAUNCHER_TABLE);
		db.execSQL(CREATE_SETTINGS_TABLE);
		db.execSQL(CREATE_DEVICEID_TABLE);
		Log.d("sqlite", CREATE_CONTACTS_TABLE);
	}

	
	
	//upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_AUTH);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PASS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ENCRPTION_KEY);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_EMAIL);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_LAUNCHER);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SETTINGS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_DEVICE_ID);
		// Create tables again
		onCreate(db);

	}

	//This is just a dummy function to fill the launcher params in database.
	//Later on the values of this database will be filled by the values given from portal
	public void createDummyLauncherDatabase(String localPwd){
		try {
			insertPkgEntry("com.dell.email", localPwd);
			insertPkgEntry("com.dell.dmpfileshare", localPwd);
			insertPkgEntry("com.dell.dmpbrowser", localPwd);
			insertPkgEntry("com.example.testbackgroundapps", localPwd);
			insertPkgEntry("com.example.testpersonalprofile",localPwd);
			insertPkgEntry("com.dell.calendar",localPwd);
			insertPkgEntry("com.dell.vpntest",localPwd);
			insertPkgEntry("com.toktumi.line2",localPwd);
			insertPkgEntry("com.wyse.filebrowserfull",localPwd);
			insertPkgEntry("com.sonicwall.mobileconnect",localPwd);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void insertPkgEntry(String aPkgName, String localPwd) throws Exception{		
		// Make sure no duplicates found
		ArrayList<String>pkgs = getPackages();
		for ( int i=0; i < pkgs.size(); i++ )
		{
			if ( pkgs.get(i).compareTo(aPkgName) == 0 )
			{
				return;
			}
		}
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		
		try {
/*			values.put(KEY_VERSION_NUM,SimpleCrypto.encrypt(localPwd ,"1.0"));
			values.put(KEY_PACKAGE_NAME,SimpleCrypto.encrypt(localPwd ,aPkgName));
			values.put(KEY_INSTALL_STATUS, SimpleCrypto.encrypt(localPwd ,"installed"));
			values.put(KEY_RESERVED,SimpleCrypto.encrypt(localPwd ,"reserved"));*/
			values.put(KEY_VERSION_NUM,"1.0");
			values.put(KEY_PACKAGE_NAME,aPkgName);
			values.put(KEY_INSTALL_STATUS, "installed");
			values.put(KEY_RESERVED,"reserved");			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		db.insert(TABLE_LAUNCHER, null, values);
		db.close();
	}	
	
	public Cursor getPkgList() throws Exception{
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor;
		
		try
		{
			cursor = db.query(TABLE_LAUNCHER, new String[] {KEY_PACKAGE_NAME},
	        		null,
	        		null,
	        		null,
	        		null,
	        		null,
	        		null
	        		);
			if (cursor != null&& cursor.getCount()>0) {
				cursor.moveToFirst();
	        }		 
		}
		catch(Exception e)
		{
			throw e;
		}
	        
		return cursor;
	}	
	
	public ArrayList<String> getPackages() throws Exception
	{		
		ArrayList<String> pkgEntries = new ArrayList<String>();	
		
		Cursor c = null;
		
		try {
			c = getPkgList();
			
			int PkgNameCol = c.getColumnIndex(KEY_PACKAGE_NAME);
			
			while (c.getPosition() < c.getCount()) {
															
				String PkgName = c.getString(PkgNameCol);							
								
				pkgEntries.add(PkgName);						
				
				c.moveToNext();	
			}
		}
		catch (Exception e) {
			throw new Exception (e);
		}
		finally {
			if (c != null) {
				try {
					c.close();
				}
				catch (Exception e) {					
				}
			}			
		}
		
		return pkgEntries;
	}
	
	public void deletePkgEntrys(String aPkgName) throws Exception{
		SQLiteDatabase db = this.getWritableDatabase();
		try
		{
			db.execSQL("DELETE FROM " + TABLE_LAUNCHER + " WHERE "+KEY_PACKAGE_NAME+"='" + aPkgName +"';");
			//db.delete(DATABASE_TABLE_PKGLIST, "PkgName=?", new String[] { aPkgName });
		}
		catch(Exception e)
		{
			throw e;
		}
		
	}
	
	public String getPkgName(String aApKName) throws Exception{
		
		String pkgname = null;
		
		Cursor c = null;
		
		try {
			c = getPkgList();
			
			int PkgNameCol = c.getColumnIndex(KEY_PACKAGE_NAME);
			int ApkNameCol = c.getColumnIndex("ApkName");
			
			while (c.getPosition() < c.getCount()) {
															
				String ApkName = c.getString(ApkNameCol);								
										
				if ( aApKName.compareTo(ApkName) == 0 )
				{
					// apk name matched. So get the package name and break;
					pkgname = c.getString(PkgNameCol);	
					break;
				}
				c.moveToNext();	
			}
		}
		catch (Exception e) {
			throw new Exception (e);
		}
		finally {
			if (c != null) {
				try {
					c.close();
				}
				catch (Exception e) {					
				}
			}			
		}
		
		return pkgname;
	}
	
	
	//ankit
	//This is just a dummy function to fill the email params in database.
	//Later on the values of this database will be filled by the values given from portal
	public void createDummyEmailDatabase(String localPwd){
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		
		try {
			values.put(KEY_EMAIL_USERNAME,SimpleCrypto.encrypt(localPwd ,"ankit@dmpmail.onmicrosoft.com"));
			values.put(KEY_EMAIL_PASSWORD,SimpleCrypto.encrypt(localPwd ,"Dell1234"));
			values.put(KEY_EMAIL_DOMAIN, SimpleCrypto.encrypt(localPwd ,"dmpmail.onmicrosoft.com"));
			values.put(KEY_EMAIL_SERVER,SimpleCrypto.encrypt(localPwd ,"pod51024.outlook.com"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		db.insert(TABLE_EMAIL, null, values);
		db.close();
	}
	
	public void setLocalPassword(String aPassword,String aTime)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_LOCAL_PASS, aPassword); // Contact Name
		values.put(KEY_LAST_ACCESS_TIME,aTime); // Contact Phone

		// Inserting Row
		db.insert(TABLE_PASS, null, values);

		db.close(); // Closing database connection
	}


	public void setEncryptionKey(String aKey)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_FSDB_ENCRYPTION_KEY, aKey); // enc key
		// Inserting Row
		db.insert(TABLE_ENCRPTION_KEY, null, values);

		db.close(); // Closing database connection
	}
	
	public String getEncryptionKey()
	{
		String lKey =null;

		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_ENCRPTION_KEY, null, null,
				null, null, null, null, null);

		if (cursor != null & cursor.getCount()>0)
			cursor.moveToFirst();
		else
		{
			if(cursor!=null)
				cursor.close();
			db.close();
			return null;
		}
		lKey = cursor.getString(cursor.getColumnIndex(KEY_FSDB_ENCRYPTION_KEY));
		
		if(cursor!=null)
			cursor.close();
		db.close();
		return lKey;
	}
	
	public String getEmailParam(String param){
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(TABLE_EMAIL, null, null,
				null, null, null, null, null);
		if (cursor != null & cursor.getCount()>0){
			cursor.moveToFirst();
		} else{
			if(cursor!=null){
				cursor.close();
			}	
			db.close();
			return null;
		}
		String columnName;
		if(param.equals("get_email_username_key")){
			columnName = KEY_EMAIL_USERNAME;
		}else if (param.equals("get_email_password_key")){
			columnName = KEY_EMAIL_PASSWORD;
		}else if (param.equals("get_email_domain_key")){
			columnName = KEY_EMAIL_DOMAIN;
		}else if (param.equals("get_email_server_key")){
			columnName = KEY_EMAIL_SERVER;
		}else{
			return null;
		}
		String str = cursor.getString(cursor.getColumnIndex(columnName));
		if(cursor!=null)
			cursor.close();
		db.close();
		return str;
	}
	
	public String getLastAccessTime()
	{

		String laTime = null;
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_PASS, null, null,
				null, null, null, null, null);

		if (cursor != null & cursor.getCount()>0)
			cursor.moveToFirst();
		else
		{
			if(cursor!=null)
				cursor.close();
			db.close();
			return laTime;
		}
		//String lPassword = cursor.getString(cursor.getColumnIndex(KEY_LOCAL_PASS));
		laTime = cursor.getString(cursor.getColumnIndex(KEY_LAST_ACCESS_TIME));
		
		if(cursor!=null)
			cursor.close();
		db.close();
		Log.v("dbhelper","before return"+" time:"+laTime);
		// return password
		//result[0] = new String(lPassword);
		//	result[1] = new String(lTime);
		//result = new String[]{lPassword,lTime};
		
		
		return laTime;
	//	return new String[]{lPassword,lTime};
	
	}
	public String getLocalPassword()
	{
		String lPassword =null;

		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_PASS, null, null,
				null, null, null, null, null);

		if (cursor != null & cursor.getCount()>0)
			cursor.moveToFirst();
		else
		{
			if(cursor!=null)
				cursor.close();
			db.close();
			return null;
		}
		 lPassword = cursor.getString(cursor.getColumnIndex(KEY_LOCAL_PASS));
//		String lTime = cursor.getString(cursor.getColumnIndex(KEY_LAST_ACCESS_TIME));
		
		if(cursor!=null)
			cursor.close();
		db.close();
		Log.v("dbhelper","before return"+lPassword);
		// return password
		//result[0] = new String(lPassword);
		//	result[1] = new String(lTime);
		//result = new String[]{lPassword,lTime};
		//result.add(lPassword);// lPassword;
		//result.add(lTime);
		
		return lPassword;
	//	return new String[]{lPassword,lTime};
	}

	
	public void clearLocalPassword() {
		SQLiteDatabase db = this.getWritableDatabase();

		db.delete(TABLE_PASS,null,null);

		db.close(); // Closing database connection
	}
	/**
	 * add username and password to database
	 * @param username
	 * @param password
	 */
	public void addPassword(String username, String password) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_NAME, username); // Contact Name
		values.put(KEY_PASSWORD,password); // Contact Phone

		// Inserting Row
		db.insert(TABLE_AUTH, null, values);

		db.close(); // Closing database connection
	}



	/**
	 * 
	 * @return password
	 */
	public String getpassword() {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_AUTH, new String[] { KEY_ID,
				KEY_NAME, KEY_PASSWORD }, null,
				null, null, null, null, null);

		if (cursor != null)
			cursor.moveToFirst();

		String password = cursor.getString(2);
		cursor.close();
		db.close();
		// return password
		return password;
	}

	/**
	 * 
	 * @return password
	 */
	public String getusername() {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_AUTH, new String[] { KEY_ID,
				KEY_NAME, KEY_PASSWORD }, null,
				null, null, null, null, null);

		if (cursor != null)
			cursor.moveToFirst();

		String username = cursor.getString(1);
		cursor.close();
		db.close();
		// return password
		return username;
	}
	
	/**
	 * add username and password to database
	 * @param username
	 * @param password
	 * @param token
	 */
	public void setToken(String username, String password,String token) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_NAME, username); // Contact username
		values.put(KEY_PASSWORD,password); // Contact password
		values.put(KEY_SSO_TOKEN,token); // Contact password

		// Inserting Row
		db.insert(TABLE_AUTH, null, values);

		db.close(); // Closing database connection
	}

	public void clearToken() {
		SQLiteDatabase db = this.getWritableDatabase();

		db.delete(TABLE_AUTH,null,null);

		db.close(); // Closing database connection
	}

	/**
	 * 
	 * @return token
	 */
	public String getToken() {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_AUTH, null, null,
				null, null, null, null, null);

		if (cursor != null & cursor.getCount()>0)
			cursor.moveToFirst();
		else
		{
			if(cursor!=null)
				cursor.close();
			db.close();
			return null;
		}
		String token = cursor.getString(cursor.getColumnIndex(KEY_SSO_TOKEN));
		if(cursor!=null)
			cursor.close();
		db.close();
		// return password
		return token;
	}
	
	
	public void setDeviceId(String aDeviceId)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		//db.execSQL("DROP TABLE IF EXISTS " + TABLE_DEVICE_ID);
		
		ContentValues values = new ContentValues();
				
		values.put(KEY_DEVICEID, aDeviceId); // USER ID RECEIVED AFTER AUTHENTICATION
//		values.put(KEY_IS_DEVICE_LOCKED,"dummy");
		// Inserting Row
		db.insert(TABLE_DEVICE_ID, null,values );
		db.close(); // Closing database connection
//		Log.d(Constants.TAG,"DB Setting::"+userid);
	}
	
	public String getDeviceId()
	{
		String lDeviceId =null;

		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_DEVICE_ID, null, null,
				null, null, null, null, null);

		if (cursor != null&& cursor.getCount()>0)
		{	cursor.moveToFirst();
			lDeviceId = cursor.getString(0);//cursor.getColumnIndex(KEY_DEVICEID));
		}

//		Log.d(Constants.TAG,"DB Getting(0)::Getting(1)"+lPassword+"::"+cursor.getString(1));
		db.close();		
		return lDeviceId;

	}

	
	
	public void setEnrolUserId(String userid)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_ENROL_USERID, userid); // USER ID RECEIVED AFTER AUTHENTICATION
//		values.put(KEY_IS_DEVICE_LOCKED,"dummy");
		// Inserting Row
		db.update(TABLE_SETTINGS, values, null, null );
		db.close(); // Closing database connection
//		Log.d(Constants.TAG,"DB Setting::"+userid);
	}
	
	public String getEnrolUserId()
	{
		String lPassword =null;

		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_SETTINGS, new String[] { KEY_ENROL_USERID,
				KEY_IS_DEVICE_LOCKED, KEY_TIME_VALUE, KEY_TIMEOUT_DETECTED }, null,
				null, null, null, null, null);

		if (cursor != null&& cursor.getCount()>0)
		{	cursor.moveToFirst();
			lPassword = cursor.getString(0);//cursor.getColumnIndex(KEY_ENROL_USERID));
		}

//		Log.d(Constants.TAG,"DB Getting(0)::Getting(1)"+lPassword+"::"+cursor.getString(1));
		db.close();		
		return lPassword;

	}

	public void setIsDeviceLocked(int value)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_IS_DEVICE_LOCKED,value);
		// Inserting Row
		db.update(TABLE_SETTINGS, values, null, null );
		db.close(); // Closing database connection
//		Log.d(Constants.TAG,"DB Setting::"+userid);
	}
	
	public boolean getIsDeviceLocked()
	{

		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_SETTINGS, new String[] { KEY_ENROL_USERID,
				KEY_IS_DEVICE_LOCKED, KEY_TIME_VALUE, KEY_TIMEOUT_DETECTED }, null,
				null, null, null, null, null);

		int x = 2;
		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			x = cursor.getInt(cursor.getColumnIndex(KEY_IS_DEVICE_LOCKED));			
			db.close();
		}
		return x == 1;

	}
	
	
	public void createDummySettingsDb(String uname) {
		// TODO Auto-generated method stub
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		
		try {
			values.put(KEY_ENROL_USERID,"dummy");
			values.put(KEY_IS_DEVICE_LOCKED,0);
			values.put(KEY_TIME_VALUE, "");
			values.put(KEY_TIMEOUT_DETECTED, 0);
			values.put(KEY_LAST_USED_AUTH_URL, "");
			values.put(KEY_USERNAME_MAIN, uname);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		db.insert(TABLE_SETTINGS, null, values);
		db.close();
	}
	
	public void setTime(String time)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_TIME_VALUE, time); // USER ID RECEIVED AFTER AUTHENTICATION
//		values.put(KEY_IS_DEVICE_LOCKED,"dummy");
		// Inserting Row
		db.update(TABLE_SETTINGS, values, null, null );
		db.close(); // Closing database connection
//		Log.d(Constants.TAG,"DB Setting::"+userid);
	}
	
	public String getTime()
	{
		String lPassword =null;

		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_SETTINGS, new String[] { KEY_ENROL_USERID,
				KEY_IS_DEVICE_LOCKED, KEY_TIME_VALUE, KEY_TIMEOUT_DETECTED }, null,
				null, null, null, null, null);

		if (cursor != null&& cursor.getCount()>0)
		{	cursor.moveToFirst();
			lPassword = cursor.getString(cursor.getColumnIndex(KEY_TIME_VALUE));
		}

//		Log.d(Constants.TAG,"DB Getting(0)::Getting(1)"+lPassword+"::"+cursor.getString(1));
		db.close();		
		return lPassword;

	}
	
	public void setAuthUrl(String aUrl)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_LAST_USED_AUTH_URL, aUrl); // USER ID RECEIVED AFTER AUTHENTICATION
//		values.put(KEY_IS_DEVICE_LOCKED,"dummy");
		// Inserting Row
		db.update(TABLE_SETTINGS, values, null, null );
		db.close(); // Closing database connection
//		Log.d(Constants.TAG,"DB Setting::"+userid);
	}
	
	public String getAuthUrl()
	{
		String lUrl =null;

		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_SETTINGS, new String[] { KEY_ENROL_USERID,
				KEY_IS_DEVICE_LOCKED, KEY_TIME_VALUE, KEY_TIMEOUT_DETECTED,KEY_LAST_USED_AUTH_URL }, null,
				null, null, null, null, null);

		if (cursor != null&& cursor.getCount()>0)
		{	cursor.moveToFirst();
			lUrl = cursor.getString(cursor.getColumnIndex(KEY_LAST_USED_AUTH_URL));
		}

//		Log.d(Constants.TAG,"DB Getting(0)::Getting(1)"+lPassword+"::"+cursor.getString(1));
		db.close();		
		return lUrl;

	}
	
	public String getUsernameMain()
	{
		String lUrl =null;

		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_SETTINGS, new String[] { KEY_ENROL_USERID,
				KEY_IS_DEVICE_LOCKED, KEY_TIME_VALUE, KEY_TIMEOUT_DETECTED,KEY_LAST_USED_AUTH_URL,KEY_USERNAME_MAIN }, null,
				null, null, null, null, null);

		if (cursor != null&& cursor.getCount()>0)
		{	cursor.moveToFirst();
			lUrl = cursor.getString(cursor.getColumnIndex(KEY_USERNAME_MAIN));
		}

//		Log.d(Constants.TAG,"DB Getting(0)::Getting(1)"+lPassword+"::"+cursor.getString(1));
		db.close();		
		return lUrl;

	}	
	
	public void setTimeoutDetected(int value)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_TIMEOUT_DETECTED,value);
		// Inserting Row
		db.update(TABLE_SETTINGS, values, null, null );
		db.close(); // Closing database connection
//		Log.d(Constants.TAG,"DB Setting::"+userid);
	}
	
	public boolean getTimeoutDetected()
	{

		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_SETTINGS, new String[] { KEY_ENROL_USERID,
				KEY_IS_DEVICE_LOCKED, KEY_TIME_VALUE, KEY_TIMEOUT_DETECTED }, null,
				null, null, null, null, null);

		int x = 2;
		if (cursor != null&& cursor.getCount()>0) {
			cursor.moveToFirst();
			x = cursor.getInt(cursor.getColumnIndex(KEY_TIMEOUT_DETECTED));
			
			db.close();
		}
		return x == 1;

	}
	

}
